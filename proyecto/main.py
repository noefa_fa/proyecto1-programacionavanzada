#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import pandas as pd
import os
from Aplicacion import Aplicacion


# Procedimiento para generar el archivo
def archivo():
    archivo = pd.read_csv("IMDb movies.csv")
    return archivo


# Procedimiento que genera una lista con los titulos
def crear_titulos(app):
    for i in app.elementos:
        i.imprimir_listado()


# Procedimiento que busca titulo, fecha y genero que ingrese el usuario
def listas_busqueda(app):
    titulos = []
    fecha = []
    genero = []
    for i in app.elementos:
        titulos.append(i.get_title())
        fecha.append(i.year)
        genero.append(i.genre)
    return titulos, fecha, genero


# Procedimiento de busqueda
def buscar(lista, app):
    b = str(input("Ingrese busqueda: "))
    # cuando ingresa más de una palabra
    b = b.split(" ")
    index = []
    # Se busca lo que ingrese el usuario
    for i in range(len(lista)):
        for j in b:
            if j.upper() in str(lista[i]).upper():
                index.append(i)
                break
    # si no coincide con nada
    if index == []:
        print("No se ha encontrado lo que busca")
    else:
        for i in index:
            app.elementos[i].imprimir_movie()
        print("Cantidad de resultados: {0}".format(len(index)))


# Procedimiento que lista los países de cada película
def lista_paises(paises_pelis):
    paises = []
    for j in paises_pelis:
        try:
            # los separa por comas
            j = list(j.split(","))
            paises.append(j)
            # cuando no hay país es "float", no se usa split
        except AttributeError:
            paises.append(None)
    return paises


def crea_aplicacion(archivo):
    # Se filtra el arhivo original
    new = archivo.filter(["title", "year", "genre", "country", "actors",
                          "description", "reviews_from_users"])
    # data solo de los países
    paises_pelis = new["country"]
    # devuelve los países en listas
    paises = lista_paises(paises_pelis)
    # crea aplicación
    app = Aplicacion(new, paises)
    return app


def menu(app):
    try:
        print("Ingrese una opción")
        print("1. Ver listado\n2. Buscar película\n3. Salir")
        opcion = int(input())
        if opcion == 1:
            os.system('clear')
            crear_titulos(app)
            menu(app)
        elif opcion == 2:
            print("Busqueda por:\n1. Titulo\n2. Año\n3. Género")
            busca = int(input())
            # listas con las busquedas disponibles
            titulos, fecha, genero = listas_busqueda(app)
            if busca == 1:
                os.system('clear')
                buscar(titulos, app)
                menu(app)
            elif busca == 2:
                os.system('clear')
                buscar(fecha, app)
                menu(app)
            elif busca == 3:
                os.system('clear')
                print("Recordar que los géneros están en inglés.")
                print("Si quiere buscar comedia, tiene que buscar comedy.")
                buscar(genero, app)
                menu(app)
            else:
                print("Era 1, 2 o 3.")
                menu(app)
        elif opcion == 3:
            print("Hasta la próxima.")
        else:
            print("Era 1, 2 o 3.")
            menu(app)
    except ValueError:
        print("Opción no válida.")
        menu(app)


# Funcion principal o main
if __name__ == "__main__":
    archivo = archivo()
    app = crea_aplicacion(archivo)
    print("Bienvenido a {0}".format(app.nombre))
    print("Aquí encontrarás todas las películas chilenas!")
    menu(app)
