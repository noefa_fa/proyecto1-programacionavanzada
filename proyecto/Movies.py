#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from Elenco import Elenco


# Clase que representa a las peliculas chilenas
class Movies():

    # Constructor de la clase Movies
    def __init__(self, new, i, a):
        self.__title = new["title"][i]
        self.year = new["year"][i]
        self.country = new["country"][i]
        self.genre = new["genre"][i]
        self.description = new["description"][i]
        # Crear a los actores como objetos
        add = new["actors"][i]
        add = add.split(",")
        reparto = []
        try:
            for i in range(len(add)):
                # crea objeto e (actor)
                e = Elenco(add[i])
                reparto.append(e)
            self.actors = reparto
        except KeyError:
            pass
        self.__reviews_from_users = new["reviews_from_users"][i]
        self.index = a

    # Obtencion de los titulos
    def get_title(self):
        return self.__title

    def get_reviews(self):
        return self.__reviews_from_users

    # Procedimiento que imprime el lista de las peliculas
    def imprimir_listado(self):
        # para que al imprimir los nombres queden alineados
        s = len(list(str(self.index)))
        espacio = " " * (4 - s)
        print("{0}{1}- {2}".format(self.index, espacio, self.get_title()))

    # Procedimiento que imprime todos los datos de la pelicula seleccionada
    def imprimir_movie(self):
        espacio = "-"
        print((100 * espacio))
        print("Título: {0}".format(self.get_title()))
        print("Año: {0}".format(self.year))
        print("Género: {0}".format(self.genre))
        print("Reparto:")
        for h in self.actors:
            h.imprimir()
        print("Descripción: {0}".format(self.description))
        print("Voto de usuarios: {0}/10".format(self.get_reviews()))
        print((100 * espacio))
