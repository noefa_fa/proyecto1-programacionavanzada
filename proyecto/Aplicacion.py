#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from Movies import Movies


#  Clase que representa la aplicación con las películas
class Aplicacion():

    # Constructor de la clase aplicación
    def __init__(self, new, paises):
        self.nombre = "MovieApp"
        """
        Proceso de evaluar y sacar las películas de Chile
        """
        # lista de peliculas comienza vacía
        p = []
        # índice de película en la app
        a = 0
        for i in range(len(paises)):
            # hay películas con país desconocido
            if paises[i] is None:
                continue
            else:
                # cuando una película tiene más de un país
                for j in paises[i]:
                    # Chile está después del primero, comienza con espacio
                    if j == "Chile" or j == " Chile":
                        a += 1
                        # agrega objetos a lista
                        p.append(Movies(new, i, a))
        self.elementos = p
