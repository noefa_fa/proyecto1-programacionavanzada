#!/usr/bin/env python3
# -*- coding:utf-8 -*-


# Clase que represenata a los actores de la pelicula
class Elenco():

    # Constructor de la clase elenco
    def __init__(self, new):
        nombre = ""
        # En la lista los actores comienzan a veces con " "
        if new[0] == " ":
            i = 1
        else:
            i = 0
        for j in range(i, len(new)):
            nombre += new[j]
        self.nombre = nombre

    def imprimir(self):
        print(self.nombre)
